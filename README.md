# README #

This README will document the use of the MetaPhylogeny pipeline, which have been used in the upcomming paper "Insert title here".

### Documentation ###
The following technologies have been used for the analysis pipeline

* [R](https://www.r-project.org/)
+ R packages:
	* [tidyr](https://cran.r-project.org/web/packages/tidyr/README.html)
	* [dplyr](https://www.r-project.org/nosvn/pandoc/dplyr.html)
	* [devtools](https://www.r-project.org/nosvn/pandoc/devtools.html)
	* gdata
		* `install.packages("gdata")`
	* [ggplot2](https://www.rdocumentation.org/packages/ggplot2/versions/2.2.1)
	* [cowplot](https://cran.r-project.org/web/packages/cowplot/README.html)
	* [ggbeeswarm](https://github.com/eclarke/ggbeeswarm)



The following technologies have been used for plotting the phylogenetic trees

* [R](https://www.r-project.org/)
* [cairo graphics](https://www.cairographics.org/download/)
* R packages
	* ape
		* `install.packages("ape")`
	* [Biostrings](http://bioconductor.org/packages/release/bioc/html/Biostrings.html)
	* [scales](https://rdrr.io/cran/scales/)
	* [ggtree](https://bioconductor.org/packages/release/bioc/html/ggtree.html)
	* [treeio](https://bioconductor.org/packages/release/bioc/html/treeio.html)
	* [ggplot2](https://www.rdocumentation.org/packages/ggplot2/versions/2.2.1)
	* [cowplot](https://cran.r-project.org/web/packages/cowplot/README.html)
	* [dplyr](https://www.r-project.org/nosvn/pandoc/dplyr.html)
	* [tidyr](https://cran.r-project.org/web/packages/tidyr/README.html)
	* [svglite](https://github.com/r-lib/svglite)
	* gtools
		* `install.packages("gtools")`


### Usage ###

#### Analysis pipeline ####

The scripts used for analysis and results which the paper has visualizations of are placed in the folder `~/MetaPhylogeny_paper/analysis_pipeline/`


Usage of the analysis pipeline is based on the R script: `~/MetaPhylogeny_paper/analysis_pipeline/scripts/pipeline.R`

Before running the `pipeline.R` script in Rstudio the directory has to be set to source file.

This is done in the Rstudio menu under Session -> Set Working Directory -> To Source File Location

Note that with different file names for distance matrices and other csv files in the folder `~/MetaPhylogeny_paper/data/` the names will have to be changed in the R script.
These are all marked with the comment `# HARDCODED`



The R script: `~/MetaPhylogeny_paper/analysis_pipeline/scripts/distance_matrix_tests.R`

Has the functions for calculating the modified distance based Welch *t*-test used in the paper.

Examples of usage can be seen in the `pipeline.R` script


#### Plotting trees ####

The scripts used for plotting the newick files as phylogenetic trees are placed in the folder: `~/MetaPhylogeny_paper/plot_trees`

Along with the actual resulting trees as both pdf and svg files

Note that line 63 in the script will need to be changed to actually mimic the source file placement on the respective computer.


#### Convert raw distance matrices ####

The Python2 script `~/MetaPhylogeny_paper/convert_distance_matrices/parse_distance_matrices.py` used to convert raw distance matrice output from the metaphylogeny pipeline, to get it into a format that R can handle.

Note that the source_dir and save_dir variables should be edited.

### Data ###

The folder `~/Metaphylogeny_paper/data` contain all data which the analysis is based on.

**Subfolder `distance_matrices`**

* Contain cleaned up distance matrices that R is able to read

**Subfolder `meta_data`**

* Contain organism information and country information used in analysis

**Subfolder `phylogenetic_trees`**

* Contain newick files for each template which are an output of the metaphylogeny pipeline

**Subfolder `raw_distance_matrices`**

* Contain the metaphylogeny pipeline output distance matrices for each template.

### EDIT GUIDELINESS ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact