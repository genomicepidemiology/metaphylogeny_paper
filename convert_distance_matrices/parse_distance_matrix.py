#!/usr/bin/python

import os
import glob
import pandas as pd

###-----------------------------###
### PARSE DISTANCE MATRIX FILES ###
###-----------------------------###

## LOAD FILES ##

# directory with the raw data distance matrixes
source_dir = "~/MetaPhylogeny_paper/Data/raw_distance_matrices"

# directory to save new distance matrix format to
save_dir = "/MetaPhylogeny_paper/Data/Data/distance_matrices"

# make list of files
file_list = glob.glob(source_dir + '/*.distance')


file_name_dict = {}
for fh in file_list: # loop through and open files one by one
	f_name = fh.split("/")[-1]
	f_name = f_name.split("_gen")[0]
	print "f_name", f_name
	fh_in = open(fh, "r")
	line_count = 0 # initiale line count
	distance_matrix_list = []
	fsa_list = []
	# LOOP THROUGH LINES AND SAVE DISTANCE MATRIX VALUES ALONG WITH SAMPLES
	for line in fh_in: # loop through lines of file
		line_count += 1
		line = line.rstrip()
		# SAVE DIMENSIONS OF THE MATRIX FROM THE FIRST LINE OF THE FILE
		if line_count == 1: # first line contains number indicating the number of samples in the distance matrix
			matrix_dim = line
			file_name_dict[f_name] = matrix_dim
		# SAVE THE DISTANCE MATRIX VALUES TO LIST
		else: 
			if ".fsa" in line:
				distance_matrix_list.append([])
				fsa_list.append(line.split()[0])
				distances = line.split()[1:]
				for d in distances:
					distance_matrix_list[-1].append(d)
			else:
				distances = line.split()
				#print line
				for d in distances:
					distance_matrix_list[-1].append(d)
				#print distance_matrix_list
	# WRITE DISTANCE MATRIX TO FILE
	fh_o = open("{}/{}.csv".format(save_dir,f_name),"w")
	header = '\t'.join(fsa_list)
	fh_o.write("{}\n".format(header))
	for i in distance_matrix_list:
		w_line = '\t'.join(i)
		fh_o.write("{}\n".format(w_line))
